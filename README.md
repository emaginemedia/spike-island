# Spike Island App

# IMPORTANT
Due to a bug in iOS 9.2+ on iPhone 6 and 6s Cordova Media Plugin needs two files edited to work.

Two files that need this edit:

`platforms/ios/spikeIsland/Plugins/cordova-plugin-media/CDVSound.m`

`plugins/cordova-plugin-media/src/ios/CDVSound.m`

Around line 350 change this:
```
if (audioFile.rate != nil){
   float customRate = [audioFile.rate floatValue];
   NSLog(@"Playing stream with AVPlayer & custom rate");
   [avPlayersetRate:customRate];
} else {
   NSLog(@"Playing stream with AVPlayer & custom rate");
   [avPlayer play];
}
```
TO:
```
if (audioFile.rate != nil){
    float customRate = [audioFile.rate floatValue];
    NSLog(@"Playing stream with AVPlayer & custom rate");
    [audioFile.player setRate:customRate];
} else {
    NSLog(@"Playing stream with AVPlayer & custom rate");
    [audioFile.player play];
}

```

More info can be found here: https://forum.ionicframework.com/t/ios-9-2-cordova-plugin-media-no-sound-fix-on-the-way/45100

## Quirks
Some features such as the media player will only run on device. This is because it using Cordova plugins to leverage device features. If you try to get to a page that tries to call the media player from the controller you will get errors and the page won't open. You can just comment out parts of the code if you need to view it in browser for styling or what ever.

Also this line of code in the `index.html` will stop live reload working:
```
<meta http-equiv="Content-Security-Policy" content="default-src *; style-src 'self' 'unsafe-inline'; script-src 'self' 'unsafe-inline' 'unsafe-eval'">
```
It is needed for it to run on device though.

So lots of fun commenting out bits here and there if you are switching between browser and device. I haven't got around to adding conditionals to auto exclude these if running in browser.
# Set Up
### Ionic and Cordova
First you will need to install ionic and Cordova if you don't already have theme:

```shell
$ npm install -g cordova ionic
```
### Android Studio
You will need to have Android Studio installed: http://developer.android.com/sdk/index.html

This will probably require you to download some SDK's then, but you should be prompted from the terminal and given a command that you can just copy and paste into the terminal and it will open Studio with the required SDK's ticked for download.

### Xcode
Update Xcode to the latest version.

### Project
Next you will need to add the platforms, these are ignored in `.gitignore`, if you get errors here you may need to update npm, globally uninstall cordova and ionic, clear npm cache then reinstall ionic and cordova. If you have already run `npm install` you will need to delete the `node_modules` folder and re-run `npm install`

```shell
$ ionic platform add ios
$ ionic platform add android
```

Next you will need to `npm install`

```shell
$ npm install
```
# Development
## Browser

**NOTE:** Some features that use Cordova plugins will cause the app to fall over or give errors when testing in the browser.
See quirks section at the top for more information on these for this project.


Develop in the browser with live reload:
```sh
$ ionic serve
```

Another version, which gives you side by side views of iOS and Android so that platform specific styles can be seen.
Most if not all have been removed from Spike.

```sh
$ ionic serve --lab
```

## Android

It is best to have an Android device for testing as the emulators are VERY slow.
I would recommend this emulator if you are going to use one: https://www.genymotion.com/
It shows up as a real device which can be debugged as a connected device.

To get up and running: First either have a gennymotion virtual device running or have an Android device connected via USB.
Make sure the device has developer mode active, USB debugging enabled and that you have selected the Yes I trust this computer when it popped up.

```sh
$ ionic run android
```

The command above will build the app then run the build on the connect device.

To debug the app once connected open Chrome and go to:
```
 chrome://inspect/
```

And you should see the device and you will be able to inspect it through chrome dev tools.

## iOS
The emulator for iOS is not too bad. Debugging it is not great though.

To get it running in emulator:
```sh
$ ionic build ios
$ ionic emulate ios
```

A better way to debug it:
```sh
$ ionic build ios
$ ionic emulate ios -lc
```
`-lc` runs it in live-reload mode and outputs `console.log();` to the terminal. Handy to spot errors that are hard to see in the logs from the emulator.

# Project Overview/Notesvin
### ng-repeat vs collection-repeat
If ng-repeat is too heavy: http://ionicframework.com/docs/api/directive/collectionRepeat/. This is great for large lists of data but may be over kill for small amounts. Also note that the force refresh image would need to be used with it.

### Api's
Spike was built so that it could easily integrate with an API in the future. Just look in the factories and most if not all of the needed services have been built.

# Data

 * The data is currently supplied via JSON files that can be found in the `www/data/*`
 * Images are in the root `img/` folder.


## Timeline Data
These are examples of timeline entries:
```json
{
  "id": 1,
  "image": "img/directory/name.jpg",
  "date": 2016,
  "plural": true,
  "displayDate": "2014 - 2016",
  "title": "Long Title for Detail Page",
  "subtitle": "Card Title",
  "theme": "fortress",
  "themeName": "Island Fortress",
  "body": "<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas beatae suscipit aliquid laborum ex odit accusamus, dicta, minima laudantium aspernatur debitis, quod, eos nulla. Consequuntur quia nam dolorum, quasi consectetur!</p><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis expedita odit, explicabo quasi possimus beatae. Eaque pariatur consectetur nulla at itaque quis laboriosam cum. Magni aut nemo voluptate ab ex.</p>",
  "fact": true,
  "factBody": "<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione quibusdam, minima assumenda, consequatur harum magnam ea ipsam odit ex quo nobis, eius. Delectus accusamus quibusdam similique enim velit mollitia unde.</p>",
  "hasGallery": true,
  "galleryTitle": "Gallery Title",
    "gallery": [
      {
        "src" : "img/gallery/gallery_1.jpg",
        "thumb": "img/thumbs/thumb_1.jpg",
        "title": "Image Title A"
      },
      {
        "src" : "img/gallery/gallery_2.jpg",
        "thumb": "img/thumbs/thumb_2.jpg",
        "title": "Image Title B"
      },
      {
        "src" : "img/gallery/gallery_3.jpg",
        "thumb": "img/thumbs/thumb_3.jpg",
        "title": "Image Title C"
      },
      {
        "src" : "img/gallery/gallery_4.jpg",
        "thumb": "img/thumbs/thumb_4.jpg",
        "title": "Image Title D"
      }
    ]
}
```
## Virtual Tour Data

**.mp3** files are to be stored in `www/data/virtual-tour/audio`

```json
{
  "id": 1,
  "image": "img/monoliths/monolith_01.jpg",
  "number": 1,
  "title": "Parade Ground and Drill Shed",
  "body": "<p>The fort was designed for a garrison of 2,500 however there was normally no more than 400 soldiers based here.  Soldiers practiced military drills on the Parade Ground and smaller groups could also train in the Drill Shed now used as the café.  Underneath the Parade Ground are large water tanks that supplied the island with fresh water in an emergency and for fire fighting.</p>",
  "hasAudio": true,
  "audio": "trackname.mp3",
  "mapImage": "img/maps/location_01.jpg",
  "directions": "<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui explicabo alias, perferendis provident, voluptatem ipsam sunt soluta laborum in aut dolorum ratione aperiam velit incidunt beatae iusto numquam saepe dolor.</p>"
}
```

## Textpage Data

At the moment all entries here will be displayed in a single page, they are listed out one after the other in blocks.

```json
{
  "id": 1,
  "title": "Main Heading for Body",
  "body": "<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur atque rerum iste ea ex, voluptatum fugit! Accusantium distinctio, dicta mollitia maiores neque ea consequuntur, nobis sit ipsum libero debitis quibusdam.</p>"
}
```

### Blockquote Structure for Body of Data
```html
<blockquote><p>In this harbour, a numerous fleet of the largest ships may ride safe in all weather, and run in at all times of tide.  The best anchorage for ships of war is between Spike Island and Cobh Fort, to the eastward, but within gunshot of both.</p><footer><cite>Vallancey, Military Itinerary of Ireland (1796)</cite></footer></blockquote>
```

### More Information
* For more help use ionic --help or ionic docs
* Visit the Ionic docs: http://ionicframework.com/docs
* For routing: https://github.com/angular-ui/ui-router
