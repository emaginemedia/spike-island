
var app = angular.module('app', ['ionic', 'app.controllers', 'app.routes', 'app.services', 'app.directives', 'app.filters', 'ngCordova', 'ngFitText'])


app.run(function($ionicPlatform, $rootScope) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    // if(window.cordova && window.cordova.plugins.Keyboard) {
    //   cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    // }
    // if(window.StatusBar) {
    //   // We only really need this on iOS
    //   if($ionicPlatform.is('ios')){
    //     StatusBar.overlaysWebView(true);
    //     StatusBar.styleLightContent();
    //   }
    // }

    /*
    * Theme Set Up for Header
    * Detail page theme is taken care of in controllers
    */
    var element;
    $rootScope.$on('$stateChangeStart', function (event, next) {
        if (next.name) {
            element = angular.element(document.querySelectorAll('ion-side-menu-content ion-header-bar'));
            switch(next.name) {
                case 'menu.islandFortress':
                    element.removeClass('t-harbour');
                    element.removeClass('t-prison');
                    element.removeClass('t-home');
                    element.addClass('t-fortress');
                    break;
                case 'menu.islandPrison':
                    element.removeClass('t-fortress');
                    element.removeClass('t-harbour');
                    element.removeClass('t-home');
                    element.addClass('t-prison');
                    break;
                case 'menu.corkHarbour':
                    element.removeClass('t-fortress');
                    element.removeClass('t-prison');
                    element.removeClass('t-home');
                    element.addClass('t-harbour');
                    break;
              case 'menu.islandHome':
                  element.removeClass('t-fortress');
                  element.removeClass('t-harbour');
                  element.removeClass('t-prison');
                  element.addClass('t-home');
                  break;
                default :
                element.removeClass('t-fortress');
                element.removeClass('t-harbour');
                element.removeClass('t-prison');
                element.removeClass('t-home');
            }
        }
    });
  });

})
app.config(function($ionicConfigProvider) {
  /*
    Full list of methods
    -> http://ionicframework.com/docs/api/provider/%24ionicConfigProvider/
  */

  // Align title center for all devices
  $ionicConfigProvider.navBar.alignTitle('center');
  // Standardise back button
  $ionicConfigProvider.backButton.text('').icon('ion-android-arrow-back');
  //Keep with platform's style
  $ionicConfigProvider.views.transition('platform');

});
