angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  .state('menu', {
    url: '/side-menu21',
    templateUrl: 'templates/menu.html',
    abstract:true,
    controller: 'sideMenuCtrl'
  })

      .state('menu.fortressSpikeIsland', {
    url: '/home',
    views: {
      'side-menu21': {
        templateUrl: 'templates/fortressSpikeIsland.html',
        controller: 'fortressSpikeIslandCtrl'
      }
    }
  })

  .state('menu.islandFortress', {
    url: '/island-fortress',
    views: {
      'side-menu21': {
        templateUrl: 'templates/islandFortress.html',
        controller: 'islandFortressCtrl'
      }
    }
  })

  .state('menu.islandPrison', {
    url: '/island-prison',
    views: {
      'side-menu21': {
        templateUrl: 'templates/islandPrison.html',
        controller: 'islandPrisonCtrl'
      }
    }
  })

  .state('menu.islandHome', {
    url: '/island-home',
    views: {
      'side-menu21': {
        templateUrl: 'templates/islandHome.html',
        controller: 'islandHomeCtrl'
      }
    }
  })

  .state('menu.corkHarbour', {
    url: '/cork-harbour',
    views: {
      'side-menu21': {
        templateUrl: 'templates/corkHarbour.html',
        controller: 'corkHarbourCtrl'
      }
    }
  })

  .state('menu.islandTimeline', {
    url: '/island-timeline',
    views: {
      'side-menu21': {
        templateUrl: 'templates/islandTimeline.html',
        controller: 'islandTimelineCtrl'
      }
    }
  })

  .state('menu.islandTimeline.detail', {
    url: '/timeline-detail/:entryId',
    views: {
      'side-menu21@menu': {
        templateUrl: 'templates/islandTimelineDetail.html',
        controller: 'islandTimelineDetailCtrl'
      }
    }
  })

  .state('menu.virtualTour', {
    url: '/virtual-tour',
    views: {
      'side-menu21': {
        templateUrl: 'templates/virtualTour.html',
        controller: 'virtualTourCtrl'
      }
    }
  })

  .state('menu.virtualTourMap', {
    url: '/virtual-tour-map',
    views: {
      'side-menu21': {
        templateUrl: 'templates/virtualTourMap.html',
        controller: 'virtualTourCtrl'
      }
    }
  })

  .state('menu.virtualTourMap.detail', {
    url: '/detail/:entryId',
    views: {
      'side-menu21@menu': {
        templateUrl: 'templates/virtualTourDetail.html',
        controller: 'virtualTourDetailCtrl'
      }
    }
  })

  .state('menu.visitSpikeIsland', {
    url: '/visit',
    views: {
      'side-menu21': {
        templateUrl: 'templates/visitSpikeIsland.html',
        controller: 'visitSpikeIslandCtrl'
      }
    }
  })

$urlRouterProvider.otherwise('/side-menu21/home')



});
