angular.module('app.controllers', [])

.controller('sideMenuCtrl', function($scope, $window, $ionicPlatform) {
  // Used to set the menu width to the width of device for full screen menu
  $scope.dev_width = $window.innerWidth;
  if($ionicPlatform.is('ios')){
    var set_height = ($window.innerHeight - 64)+"px";
    $scope.menuHeight = {"height" : set_height};
  } else{
    $scope.menuHeight = "";
  }
})

.controller('fortressSpikeIslandCtrl', function($scope) {

})

.controller('islandFortressCtrl', function($scope, TimelineFactory, $ionicModal) {
  /*
  * Get entries by theme
  */
  $scope.entries = TimelineFactory.themeEntries;
  // Used for modal
  $scope.gal = [];
  TimelineFactory
    .getEntriesByTheme('fortress')
    .then(function(){
      $scope.entries = TimelineFactory.themeEntries;
    });

    /*
    * Gallery Controls
      * This at the moment appears in most controllers
      * It is all identical code, so would like to re-visit
      * this and break it out on it's own somehow
    */
    $scope.showGallery = function(index, entry) {
        $scope.activeSlide = index;
        $scope.gal = entry;
        $scope.showModal('templates/modals/image-list-popover.html');
    }

    $scope.showModal = function(templateUrl) {
        $ionicModal.fromTemplateUrl(templateUrl, {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modal = modal;
            $scope.modal.show();
        });
    }

    $scope.closeModal = function() {
        $scope.gal = [];
        $scope.modal.hide();
        $scope.modal.remove()
    };
    /* End of Gallery Controls */
})

.controller('islandPrisonCtrl', function($scope, TimelineFactory, $ionicModal) {
  /*
  * Get entries by theme
  */
  $scope.entries = TimelineFactory.themeEntries;

  TimelineFactory
    .getEntriesByTheme('prison')
    .then(function(){
      $scope.entries = TimelineFactory.themeEntries;
    });

    /*
    * Gallery Controls
      * This at the moment appears in most controllers
      * It is all identical code, so would like to re-visit
      * this and break it out on it's own somehow
    */
    $scope.showGallery = function(index, entry) {
        $scope.activeSlide = index;
        $scope.gal = entry;
        $scope.showModal('templates/modals/image-list-popover.html');
    }

    $scope.showModal = function(templateUrl) {
        $ionicModal.fromTemplateUrl(templateUrl, {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modal = modal;
            $scope.modal.show();
        });
    }

    $scope.closeModal = function() {
        $scope.gal = [];
        $scope.modal.hide();
        $scope.modal.remove()
    };
    /* End of Gallery Controls */
})

.controller('islandHomeCtrl', function($scope, TimelineFactory, $ionicModal) {
  /*
  * Get entries by theme
  */
  $scope.entries = TimelineFactory.themeEntries;

  TimelineFactory
    .getEntriesByTheme('home')
    .then(function(){
        $scope.entries = TimelineFactory.themeEntries;
    });

    /*
    * Gallery Controls
      * This at the moment appears in most controllers
      * It is all identical code, so would like to re-visit
      * this and break it out on it's own somehow
    */
    $scope.showGallery = function(index, entry) {
        $scope.activeSlide = index;
        $scope.gal = entry;
        $scope.showModal('templates/modals/image-list-popover.html');
    }

    $scope.showModal = function(templateUrl) {
        $ionicModal.fromTemplateUrl(templateUrl, {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modal = modal;
            $scope.modal.show();
        });
    }

    $scope.closeModal = function() {
        $scope.gal = [];
        $scope.modal.hide();
        $scope.modal.remove()
    };
    /* End of Gallery Controls */
})

.controller('corkHarbourCtrl', function($scope, TimelineFactory, $ionicModal) {
  /*
  * Get entries by theme
  */
  $scope.entries = TimelineFactory.themeEntries;

  TimelineFactory
    .getEntriesByTheme('harbour')
    .then(function(){
      $scope.entries = TimelineFactory.themeEntries;
    });

    /*
    * Gallery Controls
      * This at the moment appears in most controllers
      * It is all identical code, so would like to re-visit
      * this and break it out on it's own somehow
    */
    $scope.showGallery = function(index, entry) {
        $scope.activeSlide = index;
        $scope.gal = entry;
        $scope.showModal('templates/modals/image-list-popover.html');
    }

    $scope.showModal = function(templateUrl) {
        $ionicModal.fromTemplateUrl(templateUrl, {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modal = modal;
            $scope.modal.show();
        });
    }

    $scope.closeModal = function() {
        $scope.gal = [];
        $scope.modal.hide();
        $scope.modal.remove()
    };
    /* End of Gallery Controls */
})

.controller('islandTimelineCtrl', function($scope, TimelineFactory) {
  /*
  * Get all entries for Timeline
  */
  $scope.entries = TimelineFactory.entries;

  TimelineFactory
    .getEntries()
    .then(function(){
      $scope.entries = TimelineFactory.entries;
    });
})

.controller('islandTimelineDetailCtrl', function($scope, TimelineFactory, $stateParams, $rootScope, $ionicModal) {
  // $stateParams gives us access to data passed through with ui-sref and defined in the routes.js url
  // $stateParams is from ui-router

  $scope.entry = TimelineFactory.entry;

  TimelineFactory
    .getEntry($stateParams.entryId)
    .then(function(){
      $scope.entry = TimelineFactory.entry;
      // Set navbar theme for detail view
      $rootScope.mainTheme = $scope.entry.theme;
    });

  // Reset the theme so that it is not still set om timeline view
  $scope.$on('$ionicView.beforeLeave', function(){
    $rootScope.mainTheme = "";
  });

  /*
  * Gallery Controls
    * This at the moment appears in most controllers
    * It is all identical code, so would like to re-visit
    * this and break it out on it's own somehow
  */
  $scope.showGallery = function(index) {
      $scope.activeSlide = index;
      $scope.showModal('templates/modals/image-popover.html');
  }

  $scope.showModal = function(templateUrl) {
      $ionicModal.fromTemplateUrl(templateUrl, {
          scope: $scope,
          animation: 'slide-in-up'
      }).then(function(modal) {
          $scope.modal = modal;
          $scope.modal.show();
      });
  }

  $scope.closeModal = function() {
      $scope.modal.hide();
      $scope.modal.remove()
  };
  /* End of Gallery Controls */

})

.controller('virtualTourCtrl', function($scope, TourFactory) {
  /*
  * Get all entries for Virtual Tour
  */
  $scope.entries = TourFactory.entries;

  TourFactory
    .getEntries()
    .then(function(){
      $scope.entries = TourFactory.entries;
    });

})

.controller('virtualTourDetailCtrl', function($scope, $ionicPlatform, $cordovaMedia, $stateParams, TourFactory) {
  // platform ready is needed for media plugin
  $ionicPlatform.ready(function() {

    $scope.entry = TourFactory.entry;

    TourFactory
      .getEntry($stateParams.entryId)
      .then(function(){
        $scope.entry = TourFactory.entry;
        // No need to prepare audio if there is none :)
         if($scope.entry.hasAudio){
           $scope.prepareAudio();
           console.log("prepareAudio if");
        }
      });

    /*
      This is called once entry has been updated.
      This is because the above function is async/promise so it
      is firing before audio file was available
    */
    $scope.prepareAudio = function(){
      // Control logic for alternating between play/pause buttons on frontend
      $scope.showPause = false;

      /*
        Will need to do conditional for src so that if it is local that it uses
        this and if not, an else with just the url for the hosted sound bite is used
      */
      var src = 'data/virtual-tour/audio/' + $scope.entry.audio;

      if($ionicPlatform.is('android')){
        src = '/android_asset/www/' + src;
      }
      // Define our media file
      var media = $cordovaMedia.newMedia(src);

      // Optional for iOS
      var iOSPlayOptions = {
        numberOfLoops: 1,
        playAudioWhenScreenIsLocked : false
      }

      // Play our media file
      $scope.play = function(){
        $scope.showPause = true;
        if($ionicPlatform.is('android')){
          media.play();
        }else{
          // iOS quirk needs these options
          media.play(iOSPlayOptions);
        }
      }

      // Pause our media file
      $scope.pause = function(){
        $scope.showPause = false;
        media.pause();
      }
      // We want to stop the audio and release it form memory when
      // leaving this view
      $scope.$on('$ionicView.beforeLeave', function(){
        media.stop();
        media.release();
      });
    };

  });
})

.controller('visitSpikeIslandCtrl', function($scope, TextPageFactory, $ionicModal) {
  /*
    Grab all entries for textpages.
    At the moment the textpages are not pages but rather just repeated
    blocks of content on a single page. I have set it up this way
    so that in the future, it would be very simple to add detail views
    if needed. You would just replicate something like the timeline structure.
  */
  $scope.entries = TextPageFactory.entries;

  TextPageFactory
    .getEntries()
    .then(function(){
      $scope.entries = TextPageFactory.entries;
    });

})
