angular.module('app.services', [])

.factory('TimelineFactory',[ '$http', function TimelineFactory($http){

  TimelineFactory.entries = [];
  TimelineFactory.themeEntries = [];
  TimelineFactory.entry = {};

  // Get all Timeline Data
  TimelineFactory.getEntries = function(){
    return $http.get('data/timeline/timeline.json')
    .success(function(data){
      TimelineFactory.entries = data;
    })
    .error(function(){
      console.log("TimelineFactory Error");
    });
  };

  /*
  * Get Detail via id
  */
  TimelineFactory.getEntry = function(entryId){
    return $http.get('data/timeline/timeline.json')
    .success(function(data){
      // Loop through file to get object by id
      // This shouldn't need to be done if using an api
      data.forEach(function(item, index){
        if(item.id == entryId){
          TimelineFactory.entry = item;
        }
      });
    })
    .error(function(){
      console.log("TimelineFactory Error");
    });
  };

  /*
  * Get Detail via theme
  */
  TimelineFactory.getEntriesByTheme = function(theme){
    return $http.get('data/timeline/timeline.json')
    .success(function(data){
      TimelineFactory.themeEntries = [];
      data.forEach(function(item, index){
        if(item.theme == theme){
          TimelineFactory.themeEntries.push(item);
        }
      });
    })
    .error(function(){
      console.log("TimelineFactory Error");
    });
  };

  return TimelineFactory;

}])

.factory('TourFactory',[ '$http', function TourFactory($http){

  TourFactory.entries = [];
  TourFactory.entry = {};

  // Get all Timeline Data
  TourFactory.getEntries = function(){
    return $http.get('data/virtual-tour/tour.json')
    .success(function(data){
      TourFactory.entries = data;
    })
    .error(function(err){
      console.log("TourFactory Error: getEntries | " + err);
    });
  };

  /*
  * Get Detail via id
  */
  TourFactory.getEntry = function(entryId){
    return $http.get('data/virtual-tour/tour.json')
    .success(function(data){
      // Loop through file to get object by id
      // This shouldn't need to be done if using an api
      data.forEach(function(item, index){
        if(item.id == entryId){
          TourFactory.entry = item;
        }
      });
    })
    .error(function(err){
      console.log("TourFactory Error: getEntry | " + err);
    });
  };

  return TourFactory;

}])

.factory('TextPageFactory',[ '$http', function TextPageFactory($http){

  TextPageFactory.entries = [];
  TextPageFactory.entry = {};

  // Get all Timeline Data
  TextPageFactory.getEntries = function(){
    return $http.get('data/text-pages/textpages.json')
    .success(function(data){
      TextPageFactory.entries = data;
    })
    .error(function(err){
      console.log("TextPageFactory Error: getEntries | " + err);
    });
  };

  /*
  * Get Detail via id
  * Currently not used!!
  */
  TextPageFactory.getEntry = function(entryId){
    return $http.get('data/text-pages/textpages.json')
    .success(function(data){
      // Loop through file to get object by id
      // This shouldn't need to be done if using an api
      data.forEach(function(item, index){
        if(item.id == entryId){
          TextPageFactory.entry = item;
        }
      });
    })
    .error(function(err){
      console.log("TextPageFactory Error: getEntry | " + err);
    });
  };

  return TextPageFactory;

}]);
