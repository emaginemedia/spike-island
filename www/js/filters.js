angular.module('app.filters', [])
/*
	This filter is applied to ng-bind-html to allow the html
	to be rendered as html. This is basically saying the code
	is from a trusted source.
*/
.filter('renderHTMLCorrectly', function($sce){
	return function(stringToParse)	{
		return $sce.trustAsHtml(stringToParse);
	}
});
